## HQ Music Downloader Bot for Telegram

A Telegram bot to download audio from Youtube and similar sites (all websites
that [yt-dlp](https://github.com/yt-dlp/yt-dlp) supports) in the highest quality
possible, converted into the MP3 format.

The bot also sets ID3 tags from the media metadata whenever possible, and sets
the thumbnail as an album art.

Playlists are currently not supported.

### Deploying

Since the codebase is obviously *Heroku*-ready, I'll only provide instructions
for deploying on Heroku.  You have to modify the source code a little bit to be
able to use it outside the Heroku platform.

1. Register a new bot on Telegram and a new app on Heroku
1. Clone this repo and add your Heroku app's git remote
1. Add the `ffmpeg` buildpack to provide FFMPEG to `yt-dlp`
1. Set `BOT_TOKEN` config var to your Telegram bot token
1. Enable Heroku beta feature that provides the app name as `HEROKU_APP_NAME`
1. Push the code to Heroku

### Usage

Just send a link to the bot and it will process it.  You can use the
Telegram-provided `@vid` bot to search for Youtube videos.

### Sample

A sample bot used to exist at [@YTMusicHQBot](https://t.me/YTMusicHQBot).  It
is currently not working because Heroku no longer provides free plans.  So
consider hosting your own instance (open a pull request if you want your
instance to be listed here.)

### License

Licensed under the [MIT (Expat) license](./LICENSE).

> Copyright (C) 2023 Karam Assany (karam.assany@disroot.org)
